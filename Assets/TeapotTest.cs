﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeapotTest : MonoBehaviour {
    [System.Serializable]
    public class Lane
    {
        public Transform upTransform;
    }

    public List<Lane> lanes = new List<Lane>();
    public int laneIndex = 0;
    public Transform shipTransform;
    public Transform targetTransform;
    public float lerpSpeed = 100f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            laneIndex++;
            if (laneIndex > lanes.Count - 1)
            {
                laneIndex = 0;
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            laneIndex--;
            if (laneIndex < 0)
            {
                laneIndex = lanes.Count-1;
            }
        }
        
        targetTransform = lanes[laneIndex].upTransform;
        shipTransform.position = Vector3.Lerp(shipTransform.position, targetTransform.position, Time.deltaTime * lerpSpeed);
        //lanes[laneIndex].upTransform.position;
        shipTransform.rotation = Quaternion.Lerp(shipTransform.rotation, targetTransform.rotation, Time.deltaTime * lerpSpeed);// lanes[laneIndex].upTransform.rotation;
	}
}
