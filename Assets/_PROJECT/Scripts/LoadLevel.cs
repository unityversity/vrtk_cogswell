﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevel : MonoBehaviour
{
    public string levelToLoad = "level2";
    private void OnTriggerEnter(Collider collidee)
    {
        if (collidee.tag == "Player")
        {
            // we hit the player
            Load();
        }
    }

    public void Load()
    {
        Coins.coins = 0;
        Application.LoadLevel(levelToLoad);
    }
}
