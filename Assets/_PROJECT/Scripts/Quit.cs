﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour {

    private void OnTriggerEnter(Collider collidee)
    {
        if (collidee.tag == "Player")
        {
            // we hit the player
            Application.Quit();
        }
    }
}
