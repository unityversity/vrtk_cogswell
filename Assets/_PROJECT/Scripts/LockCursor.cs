﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockCursor : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log("LockCursor.Start()");
        Lock();
	}

    bool locked = true;
   
	
    public void Lock()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void Unlock()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Toggle()
    {
        if (locked) { Unlock(); }
        else { Lock(); }
        
    }

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            Toggle();
        }
        if (Input.GetMouseButtonDown(0))
        {
            Lock();
        }
	}
}
