﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeTag : MonoBehaviour {

    public enum CubeColor
    {
        Red,
        Green,
        Blue
    }

    public CubeColor cubeColor = CubeColor.Blue;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
