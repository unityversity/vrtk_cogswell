﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendingMachine : MonoBehaviour {
    /// <summary>
    /// The transform origin point for instantiated vended objects from the give command
    /// </summary>
	public Transform outhole;
    /// <summary>
    /// The list of items that the vending machine can produce, currently infinite suppplies
    /// </summary>
	public GameObject[] inventory;

    public int coinsInMachine = 0;
    public int coinsPricePerItem = 1;

    bool infiniteCoinsBug = false;

	/// <summary>
	/// The coin readout.
	/// </summary>
	public TMPro.TextMeshPro coinsTMPro;

	public void UpdateCoinsReadout ()
	{
		coinsTMPro.text = coinsInMachine.ToString();
	}
	public Transform coinSlotSnapZone;

	/// <summary>
	/// Implements SnapDropZoneUnityEvent from the vending machine coin slot.
	/// </summary>
	public void SnapInsertCoin()
	{
		InsertValidCoins (1);
		GameObject.Destroy (coinSlotSnapZone.GetComponentInChildren<VRTK.VRTK_InteractableObject> ().gameObject);


	}

	public void InsertValidCoins(int coinsArgument)
	{
		coinsInMachine += coinsArgument;
		CmdLine.Log("INSERTED " + coinsArgument + " COINS");
		if (!infiniteCoinsBug)
		{
			Coins.coins -= coinsArgument;
		}
		UpdateCoinsReadout ();

	}

    /// <summary>
    /// Inserts a coin or coins if an argument is added
    /// </summary>
    /// <param name="args">first arg is command name, second should be a number like 3</param>
    void CommandInsert(string[] args)
    {
        int coinsArgument = 0;
        bool validNumber = true;
        if (args.Length == 1)
        {
            coinsArgument = 1;
        }
        else
        {
            if (!int.TryParse(args[1], out coinsArgument) && coinsArgument > 0)
            {
                CmdLine.Log(args[1] + " is not a positive whole number. Try again.");
                validNumber = false;
            }
        }
        
        if (validNumber)
        {
			if (Coins.coins < coinsArgument)
			{
				CmdLine.Log("You don't have " + coinsArgument + " coins to insert.");
				validNumber = false;
			}
			else
			{
				InsertValidCoins (coinsArgument);
			}
        }
        CmdLine.Log("COINS IN MACHINE: " + coinsInMachine);
    }



    /// <summary>
    /// give command takes a string argument of a type of thing to give
    /// </summary>
    /// <param name="args"></param>
	void CommandGive(string[] args)
    {
        // give you the thing you want
		if (args.Length > 1)
        {
			CmdLine.Log ("You want me to " + args [0] + " a " + args [1]);
			bool foundit = false;
			for (int i = 0; i < inventory.Length; ++i)
            {
				if (inventory [i].name == args [1])
                {
                    bool giveIt = false;
                    // it can only return as many coins as the machine has in it
                    if (inventory[i].name == "coin")
                    {
                        if (coinsInMachine>0)
                        {
                            giveIt = true;
                            coinsInMachine--;
                            CmdLine.Log("Returning coin.");
                        }
                    }
                    else
                    {
                        // it can give everything else if there's a coin in it
                        if (coinsInMachine > 0)
                        {
                            giveIt = true;
                            coinsInMachine--;
                            CmdLine.Log("COIN SPENT. GIVING ITEM.");
                        }
                        else
                        {
                            CmdLine.Log("INSERT COIN TO GIVE ITEM");
                            foundit = true;
                        }
                    }

                    if (giveIt)
                    {
                        Instantiate(inventory[i], outhole.position, outhole.rotation);
						UpdateCoinsReadout ();
                        foundit = true;
                    }
				}
			}
            
			if (!foundit)
            {
                // make a list of options in case you got the wrong thing and need suggestions
                string list = "";
                for (int i = 0; i < inventory.Length; ++i)
                {
                    if (i > 0)
                    {
                        list += ", ";
                    }
                    list += inventory[i].name;
                }
                CmdLine.Log ("Vending machine doesn't contain anything named '"+args[1]+"' is. I know "+list);
            }
		}
        else
        {
			CmdLine.Log ("usage: give <object to give>");
		}
	}


	void Start () {
		Debug.Log ("Hello World!");
		CmdLine.Log ("Hello Cogswell [pandering]");
		CmdLine.AddCommand ("give", CommandGive, "causes the vending machine to give");
		CmdLine.AddCommand ("insert", CommandInsert, "inserts a coin into a machine");


	}
	
	void Update () {
	}
}
