﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// GrabRay checks the Reticle for a target and lets us grab it
/// </summary>
public class GrabRay : MonoBehaviour {

    /// <summary>
    /// The reticle this grab ray uses
    /// </summary>
    public Reticle reticle;
    /// <summary>
    /// Where we put the object while grabbing it
    /// </summary>
    public Transform grabHandTransform;
    public bool grabbing = false;
    public GameObject grabbedItem;
    public bool temporarilyKinematic = false;
    public bool parentGrabbedObject = true;
    public bool translateGrabbedObject = false;
    public bool showLaserBeam = false;
    public LineRenderer laserBeamRenderer;
    public bool grabUsingVRTK = true;
    
	// Use this for initialization
	void Start () {
		if (reticle == null)
        {
            reticle = GetComponent<Reticle>();
        }
	}
	
    
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.T))
        {
            GameObject rightHand = GameObject.Find("RightHand");
            rightHand.transform.position = grabHandTransform.position;
            rightHand.transform.rotation = transform.rotation;
        }
        // First deal with things we already grabbed, regardless of raycasting
        if (grabbing)
        {
            if (Input.GetMouseButtonDown(0))
            {
                // drop
                Rigidbody grabbedRigidbody = grabbedItem.GetComponent<Rigidbody>();
                if (grabbedRigidbody != null)
                {
                    if (temporarilyKinematic)
                    {
                        grabbedRigidbody.isKinematic = false;
                        temporarilyKinematic = false;
                    }
                    if (parentGrabbedObject)
                    {
                        grabbedItem.transform.parent = null;
                    }
                }
                grabbing = false;
                grabbedItem = null;
                ReticleDot.Instance.show = true;
            }
            else
            {
                ReticleDot.Instance.show = false;
            }


        }
        else
        {
            // if we're not already grabbing something, check for something new

            // check what the ray hit
            if (reticle.hit)
            {
                VRTK.VRTK_InteractableObject vrtkInteractble = reticle.target.GetComponent<VRTK.VRTK_InteractableObject>();
                if (vrtkInteractble != null && vrtkInteractble.isGrabbable)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (!grabbing)
                        {
                            if (grabUsingVRTK)
                            {
                                
                            }
                            else
                            {
                                grabbing = true;
                                grabbedItem = reticle.target.gameObject;
                                Rigidbody grabbedRigidbody = grabbedItem.GetComponent<Rigidbody>();
                                if (grabbedRigidbody != null)
                                {
                                    if (!grabbedRigidbody.isKinematic)
                                    {
                                        temporarilyKinematic = true;
                                        grabbedRigidbody.isKinematic = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // Nothing hit
            }
        }
        

        // hold onto things we already have
        if (grabbing)
        {
            if (translateGrabbedObject)
            {
                grabbedItem.transform.position = grabHandTransform.position;
            }
            else if (parentGrabbedObject)
            {
                grabbedItem.transform.parent = grabHandTransform;
            }
        }

    }
}
