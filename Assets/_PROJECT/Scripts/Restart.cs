﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Restart : MonoBehaviour
{
    private void OnTriggerEnter(Collider collidee)
    {
        if (collidee.tag == "Player")
        {
            // we hit the player
            RestartLevel();
        }
    }

    public static void RestartLevel()
    {
        Coins.coins = 0;
        Application.LoadLevel(Application.loadedLevel);
    }
}
