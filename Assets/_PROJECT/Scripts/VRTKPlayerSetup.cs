﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRTKPlayerSetup : MonoBehaviour {
    public GameObject playerCollider;
    public bool done = false;
	// Use this for initialization
	void LateUpdate () {
        if (!done)
        {
            playerCollider = GameObject.Find("[VRTK][AUTOGEN][BodyColliderContainer]");
            playerCollider.tag = "Player";
            done = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
