﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    public AudioClip pickupClip;
    public AudioSource audio;
    public Renderer rendererHidden;
    public Collider colliderHidden;

    private void Start()
    {
        if (rendererHidden==null)
        {
            rendererHidden = GetComponent<Renderer>();
        }
        if(colliderHidden==null)
        {
            colliderHidden = GetComponent<Collider>();
        }
        if(audio==null)
        {
            audio = GetComponent<AudioSource>();
            if (audio==null)
            {
                audio = gameObject.AddComponent<AudioSource>();
            }
        }
    }
    private void OnTriggerEnter(Collider collidee)
    {
        if (collidee.tag == "Player")
        {
            Coins.AddCoin();
            // make it vanish
            audio.PlayOneShot(pickupClip);
            // this.gameObject is implied
            rendererHidden.enabled = false;
            colliderHidden.enabled = false;
        
        }
    }
}