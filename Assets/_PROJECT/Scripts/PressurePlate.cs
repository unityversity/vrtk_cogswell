﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour {

    public GameObject targetObject;
    public int timesTriggered = 0;
    public int timesAllowedToTrigger = 1;
    public bool deactivateTargetOnStart = false;
    public CubeTag.CubeColor switchColor = CubeTag.CubeColor.Blue;

    /// <summary>
    /// An enum is a typo-proof way of selecting more than true/false more safely than strings
    /// </summary>
    public enum Mode
    {
        Activate = 0,
        Deactivate = 1,
        Message = 2
    }

    public Mode mode = Mode.Deactivate;

    public string messageString = "DoActivateTrigger";

    private void Start()
    {
        if (deactivateTargetOnStart)
        {
            targetObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider collidee)
    {
        if (collidee.GetComponent<CubeTag>() != null
                && collidee.GetComponent<CubeTag>().cubeColor == switchColor)
        {
            if (mode == Mode.Deactivate)
            {
                timesTriggered++;
                if (timesAllowedToTrigger >= timesTriggered)
                {
                    // we hit the player
                    targetObject.SetActive(false);
                }
            }
            else if (mode == Mode.Activate)
            {
                timesTriggered++;
                if (timesAllowedToTrigger >= timesTriggered)
                {
                    // we hit the player
                    targetObject.SetActive(true);
                }
            }
            else if (mode == Mode.Message)
            {
                targetObject.SendMessage(messageString);
            }
        }
    }
}
