﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttach : MonoBehaviour {

    public bool delayAttachment = true;
    public bool attached = false;
    public bool attachToCamera = true;
    public Camera camera;

	// Use this for initialization
	void Start () {
        Attach();
	}
	
	// Update is called once per frame
	void Update () {
        if (!attached && delayAttachment)
        {
            Attach();
        }
		
	}

    public void Attach()
    {
        if (camera == null)
        {
            camera = Camera.main;
        }
        if (!attached && camera != null)
        {
            transform.parent = camera.transform;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            attached = true;
        }
    }
}
