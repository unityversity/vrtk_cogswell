﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// A class like Coins is a type of thing that can contain variables which are names for values and methods which are names for instructions to do things.
// The parts of the class are called its "members" (confusingly) which really means its variables and methods.
/// <summary>
/// Coins contains a global tally of the players' inventory of coins and methods to add and subtract from it.
/// </summary>
public class Coins : MonoBehaviour {

    // a variable stores a piece of information called a 'value' under a name we give it, called the variable name.
    // In this case, we have a variable called coins that is of an integer number type and is set to 0 initially.

    #region variables
    public static int coins = 0;

    private static int totalCoinsEverFound = 0;
    #endregion

    #region methods
    // public or private
    // the type of value that it returns (void if it doesn't return anything)
    // the name of the method
    // the arguments ( the type of each argument  the name of the argument , more arguments etc)
    public static void AddCoins(int numberToAdd)
    {
        if (numberToAdd > 0)
        {
            coins += numberToAdd;
            totalCoinsEverFound += numberToAdd;
            CmdLine.Log("COINS: " + coins);
        }
        else
        {
            Debug.LogWarning("Someone is trying to trick us by adding negative coins!");
        }
    }

    public static void AddCoin()
    {
        AddCoins(1);
    }
    #endregion methods
    
}
