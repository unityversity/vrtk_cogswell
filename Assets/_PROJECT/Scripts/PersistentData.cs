﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentData : MonoBehaviour {
    public int points = 0;

	// Use this for initialization
	void Start () {
        Object.DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
        points++;
	}
}
