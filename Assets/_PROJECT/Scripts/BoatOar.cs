﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class BoatOar : MonoBehaviour {
    public VRTK.VRTK_InteractableObject oarVRTK;
    public float rowSpeedMultiplier = 100f;
    public GameObject backgroundGO;
    public Vector3 previousOarPosition;
    public float distanceOarMovedSinceLastFrame = 0;
    public float totalDistanceMoved = 0;
    // Use this for initialization
    void Start () {
        previousOarPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//if (oarVRTK.grabbing)
        {
            distanceOarMovedSinceLastFrame = Vector3.Distance(previousOarPosition, transform.position);
            totalDistanceMoved += distanceOarMovedSinceLastFrame;
            float force = Time.deltaTime * rowSpeedMultiplier * distanceOarMovedSinceLastFrame;
            backgroundGO.transform.Translate(backgroundGO.transform.forward * force);
        }
	}
}
