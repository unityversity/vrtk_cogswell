﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticleDot : MonoBehaviour {

    public static ReticleDot Instance;

    public Reticle reticle;
    public bool show = false;
    public GameObject laserDotGO;

    void UpdateDotPosition()
    {
        if (laserDotGO != null)
        {
            // show laser dot or beam
            if (show)
            {
                laserDotGO.transform.position = reticle.raycastHit.point;
                laserDotGO.transform.rotation = transform.rotation;
                // bring it a bit closer to use to avoid Z fighting
                laserDotGO.transform.Translate(-transform.forward * 0.05f, Space.World);
            }
            else
            {
                laserDotGO.SetActive(false);
            }
        }
    }

    #region MonoBehaviour
    // Use this for initialization

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        Instance = this;
    }

    void Start () {
        if (reticle == null)
        {
            reticle = GetComponent<Reticle>();
        }
    }
	
	// Update is called once per frame
	void Update () {
        UpdateDotPosition();
	}
    #endregion MonoBehaviour
}
