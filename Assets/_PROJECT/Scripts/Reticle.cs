﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Aim a raycast forwards and indicate what it hits
/// </summary>
public class Reticle : MonoBehaviour {
    public RaycastHit raycastHit;
    public bool hit = false;
    public Collider target;
    public float maxRange = 0.5f;
    public bool newTarget = false;

    // Use this for initialization
    void Start () {
		
	}

	// Update is called once per frame
	void Update () {
        RaycastHit oldObject = raycastHit;
        raycastHit = new RaycastHit();
        if (Physics.Raycast(transform.position, transform.forward, out raycastHit, maxRange))
        {
            hit = true;
            target = raycastHit.collider;
            Debug.Log(raycastHit.collider.gameObject.name);
        }
        else
        {
            hit = false;
            target = null;
        }
	}
}
